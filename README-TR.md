# Tek Sayfa Koruma Modülü

## Açıklama
Tek Sayfa Koruma modülü, belirli sayfaları şifre ile korumanıza olanak tanır. Bu, belirli içeriklere erişimi kısıtlamak isteyen ancak tam bir kullanıcı doğrulama sistemi kurmak istemeyen Drupal siteleri için idealdir.

## Kurulum
1. Modülü Drupal'ın modüller dizinine indirin.
2. Modülü Drupal yönetim arayüzünden veya `drush en single_page_protection` komutu ile Drush üzerinden etkinleştirin.
3. Etkinleştirildikten sonra, modülü `/admin/config/single-page-protection/settings` URL'si altından yapılandırabilirsiniz.

## Yapılandırma
- Modülün yapılandırma sayfasına giderek belirli sayfalar için şifreler ayarlayın.
- URL'leri ve karşılık gelen şifreleri eklemek için formu kullanın.

## Dosyaların Genel Bakışı
- `single_page_protection.info.yml`: Modülün adını, açıklamasını, paketini ve bağımlılıklarını tanımlar.
- `single_page_protection.install`: Veritabanı şemasını oluşturmak için hook_schema'yı ve kurulum ve kaldırma işlemleri için hook'ları uygular.
- `single_page_protection.links.menu.yml`: Modülün ayarlar sayfası için yönetim menü bağlantılarını tanımlar.
- `single_page_protection.module`: Ana modül işlevselliğini içerir (şu anda boş).
- `single_page_protection.routing.yml`: Modülün ayarlar ve şifre giriş formu için yolları tanımlar.
- `single_page_protection.services.yml`: Modül tarafından kullanılan hizmetleri ve olay abonelerini kaydeder.

## Ek Bilgiler
Daha fazla yardım veya modüle katkıda bulunmak için lütfen Drupal.org'daki modül sayfasını ziyaret edin veya bakım sorumlusu ile iletişime geçin.
