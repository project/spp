# Single Page Protection Module

## Description
The Single Page Protection module allows you to protect specific pages with a password. This is ideal for Drupal sites that need to restrict access to certain content without setting up a full user authentication system.

## Installation
1. Download the module to your Drupal's modules directory.
2. Enable the module through the Drupal administration interface or via Drush with `drush en single_page_protection`.
3. Once enabled, you can configure the module under the URL `/admin/config/single-page-protection/settings`.

## Configuration
- Navigate to the module's configuration page to set up passwords for specific pages.
- Use the form to add URLs and their corresponding passwords.

## Files Overview
- `single_page_protection.info.yml`: Defines the module's name, description, package, and dependencies.
- `single_page_protection.install`: Implements hook_schema for creating the database schema, and hooks for install and uninstall processes.
- `single_page_protection.links.menu.yml`: Defines admin menu links for the module's settings page.
- `single_page_protection.module`: Contains main module functionalities (currently empty).
- `single_page_protection.routing.yml`: Defines routes for the module's settings and password entry form.
- `single_page_protection.services.yml`: Registers services and event subscribers used by the module.

## Additional Information
For further assistance or to contribute to the module, please visit the module's page on Drupal.org or contact the maintainer.
