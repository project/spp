<?php

namespace Drupal\single_page_protection\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds a new protected page.
 */
class SinglePageProtectionConfigForm extends ConfigFormBase {

  /**
   * The database connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new SinglePageProtectionConfigForm object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
        Connection $database,
        MessengerInterface $messenger
    ) {
    $this->database = $database;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'single_page_protection_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'single_page_protection.settings',
    ];
  }

  /**
   * Builds the configuration form for managing protected pages.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['protected_pages'] = [
      '#type' => 'table',
      '#header' => [$this->t('URL'), $this->t('Password'),
        $this->t('Actions'),
      ],
      '#empty' => $this->t('No protected pages yet.'),
    ];

    // Load and list protected pages from the database.
    $protected_pages = $this->loadProtectedPages();
    foreach ($protected_pages as $pid => $page) {
      $form['protected_pages'][$pid]['url'] = [
        '#type' => 'textfield',
        '#default_value' => $page['url'],
        '#disabled' => TRUE,
      ];
      $form['protected_pages'][$pid]['password'] = [
        '#type' => 'textfield',
        '#default_value' => $page['password'],
        '#disabled' => TRUE,
      ];
      $form['protected_pages'][$pid]['actions'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete'),
        '#submit' => ['::deleteProtectedPage'],
        '#name' => 'delete-' . $pid,
      ];
    }

    $form['new_protected_page'] = [
      '#type' => 'details',
      '#title' => $this->t('Add new single page protection'),
      '#open' => TRUE,
    ];

    $form['new_protected_page']['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
    ];

    $form['new_protected_page']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
    ];

    $form['new_protected_page']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#submit' => ['::addProtectedPage'],
    ];

    return $form;
  }

  /**
   * Loads protected pages from the database.
   */
  private function loadProtectedPages() {
    $query = $this->database->select('single_page_protection', 'n')
      ->fields('n', ['pid', 'url', 'password'])
      ->execute();

    $results = [];
    foreach ($query as $record) {
      $results[$record->pid] = [
        'url' => $record->url,
        'password' => $record->password,
      ];
    }
    return $results;
  }

  /**
   * Deletes a protected page.
   */
  public function deleteProtectedPage(array &$form, FormStateInterface $form_state) {
    $pid = $form_state->getTriggeringElement()['#name'];

    $this->database->delete('single_page_protection')
      ->condition('pid', str_replace('delete-', '', $pid))
      ->execute();

    $this->messenger->addMessage($this->t('Protected page deleted.'));
  }

  /**
   * Adds a new protected page.
   */
  public function addProtectedPage(array &$form, FormStateInterface $form_state) {
    $url_input = $form_state->getValue('url');
    $password_input = $form_state->getValue('password');

    $url = !is_null($url_input) ? trim($url_input) : '';
    $password = !is_null($password_input) ? trim($password_input) : '';

    if (!empty($url) && !empty($password)) {
      $this->database->insert('single_page_protection')
        ->fields([
          'url' => $url,
          'password' => $password,
        ])
        ->execute();

      $this->messenger->addMessage($this->t('New protected page added.'));
    }
    else {
      $this->messenger->addError(
            $this->t('URL and Password cannot be empty.')
        );
    }
  }

  /**
   * Adds a new protected page.
   */
  public static function create(ContainerInterface $container) {
    return new SinglePageProtectionConfigForm($container->get('database'), $container->get('messenger'));
  }

}
