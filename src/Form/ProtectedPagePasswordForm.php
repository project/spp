<?php

namespace Drupal\single_page_protection\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a form for entering a password to access a protected page.
 */
class ProtectedPagePasswordForm extends FormBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new ProtectedPagePasswordForm.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(Connection $database,
                              MessengerInterface $messenger) {
    $this->database = $database;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'single_page_protection_password_form';
  }

  /**
   * Builds the password entry form for accessing a protected page.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string|null $protected_page
   *   The path of the protected page.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form,
                            FormStateInterface $form_state,
                            Request $request = NULL) {
    $protected_page = $request->query->get('path');
    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    // Store the URL of the protected page in the form state for later use.
    $form_state->set('protected_page', $protected_page);

    return $form;
  }

  /**
   * Form submission handler.
   *
   * Verifies the entered password against the
   * stored value for the protected page.
   * If correct, grants access to the page; otherwise, displays an error.
   *
   * @param array &$form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $protected_page = $form_state->get('protected_page');
    $input_password = $form_state->getValue('password');
    $result = $this->database->select('single_page_protection', 'n')
      ->fields('n', ['url', 'password'])
      ->condition('url', $protected_page)
      ->execute()
      ->fetchAssoc();
    if ($result && ($input_password === $result['password'])) {
      // Correct password, grant access to the protected page.
      $_SESSION['single_page_protection'][$protected_page] = TRUE;
      $redirect_url = Url::fromUserInput('/' . ltrim($protected_page, '/'));
      $form_state->setRedirectUrl($redirect_url);
    }
    else {
      // Incorrect password, display an error message.
      $this->messenger->addError($this->t('Incorrect password.'));
    }
  }

  /**
   * Adds a new protected page.
   */
  public static function create(ContainerInterface $container) {
    return new ProtectedPagePasswordForm($container->get('database'),
          $container->get('messenger'));
  }

}
