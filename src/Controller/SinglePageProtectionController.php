<?php

namespace Drupal\single_page_protection\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for handling the access to protected pages.
 */
class SinglePageProtectionController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a new SinglePageProtectionController object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $database = $container->get('database');
    return new SinglePageProtectionController($database);
  }

  /**
   * Handles access control for protected pages.
   *
   * Checks if the current page is protected and prompts
   * the user for a password if necessary.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Drupal\Core\Form\FormInterface|\Symfony\Component\HttpFoundation\RedirectResponse
   *   The password form if the page is protected, or a redirect response.
   */
  public function handlePageAccess(Request $request) {
    $current_path = $request->getPathInfo();
    $protected_page = $this->getProtectedPageInfo($current_path);

    if ($protected_page) {
      // Prompt the user for the password.
      return $this->formBuilder()
        ->getForm(
              'Drupal\single_page_protection\Form\ProtectedPagePasswordForm',
              $protected_page);
    }

    // If the page is not protected, allow normal access.
    return new RedirectResponse(Url::fromRoute('<front>')->toString());
  }

  /**
   * Retrieves protected page information from the database.
   *
   * @param string $path
   *   The current page path.
   *
   * @return array|null
   *   The protected page information or NULL if the page is not protected.
   */
  private function getProtectedPageInfo($path) {
    $result = $this->database->select('single_page_protection', 'n')
      ->fields('n', ['url', 'password'])
      ->condition('url', $path)
      ->execute()
      ->fetchAssoc();

    return $result ?: NULL;
  }

}
