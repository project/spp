<?php

namespace Drupal\single_page_protection\EventSubscriber;

use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber for checking protected pages.
 */
class PageRequestSubscriber implements EventSubscriberInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a new PageRequestSubscriber.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkProtectedPage', 100];
    return $events;
  }

  /**
   * Checks if the current page is protected and manages access accordingly.
   *
   * Redirects to the password form if the page is protected and the password
   * has not been entered. Allows access if the password is validated.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request event.
   */
  public function checkProtectedPage(RequestEvent $event) {
    $request = $event->getRequest();
    $current_path = $request->getPathInfo();

    // Check if the page is protected.
    $protected_page = $this->getProtectedPageInfo($current_path);

    if ($protected_page
        && empty($_SESSION['single_page_protection'][$current_path])) {
      $password_form_url =
          Url::fromRoute('single_page_protection.password_form',
          ['path' => $current_path])->toString();
      $response = new RedirectResponse($password_form_url);
      $event->setResponse($response);
    }
  }

  /**
   * Retrieves information about a protected page.
   *
   * Checks if the URL is protected and retrieves the associated password.
   *
   * @param string $path
   *   The path of the request.
   *
   * @return array|null
   *   An associative array of protected page information
   */
  private function getProtectedPageInfo($path) {
    $result = $this->database->select('single_page_protection', 'n')
      ->fields('n', ['url', 'password'])
      ->condition('url', $path)
      ->execute()
      ->fetchAssoc();

    return $result ?: NULL;
  }

}
